"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#



def shift_list (hours_as_dec):
    hour = int(hours_as_dec)
    minute = (hours_as_dec*60) % 60
    second = int((hours_as_dec * 3600) % 60)
    answer = [hour, minute, second]

    return [hour, minute, second]


def shift_time (start_time, shift_list):
    #print("shift list: " + str(shift_list))
    hours_fixed = start_time.shift(hours = (shift_list[0]))
    minutes_fixed = hours_fixed.shift(minutes =  (shift_list[1]))
    seconds_fixed = minutes_fixed.shift(seconds = (shift_list[2]))
    return seconds_fixed

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    dist = control_dist_km
    brev_dist = brevet_dist_km
    start_time = arrow.get(brevet_start_time)
    retval = None
    shift_amt = 0
    if (dist > brev_dist and dist <= (brev_dist*1.2)): # <<maybe error catching later
        if brev_dist == 200:
            shift_amt = shift_list(brev_dist/34)
        elif brev_dist == 300:
            shift_amt = shift_list(200/34 + (brev_dist-200)/32)
        elif brev_dist == 400:
            shift_amt = shift_list(200/34 + (brev_dist-200)/32)
        elif brev_dist == 600:
            shift_amt = shift_list(200/34 + 200/32 + (brev_dist-400)/30)
        elif brev_dist == 1000:
            shift_amt = shift_list(200/34 + 200/32 + 200/30 + (brev_dist-600)/28)
    else:
        if dist <= 200:
            shift_amt = shift_list(dist/34)
        elif dist <= 400:
            shift_amt = shift_list(200/34 + (dist-200)/32)
        elif dist <= 600:
            shift_amt = shift_list(200/34 + 200/32 + (dist-400)/30)
        elif dist <= 1000:
            shift_amt = shift_list(200/34 + 200/32 + 200/30 + (dist-600)/28)
    
    return (shift_time(start_time, shift_amt)).isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    dist = control_dist_km
    brev_dist = brevet_dist_km

    start_time = arrow.get(brevet_start_time)
    retval = None
    shift_amt = 0
    if (dist == brev_dist or (dist > brev_dist and dist <= (brev_dist*1.2))):
        if brev_dist == 200:
            shift_amt = shift_list(13.5)
        elif brev_dist == 300:
            shift_amt = shift_list(20)
        elif brev_dist == 400:
            shift_amt = shift_list(27)
        elif brev_dist == 600:
            shift_amt = shift_list(40)
        elif brev_dist == 1000:
            shift_amt = shift_list(75)
    else:
        if dist <= 60:
            shift_amt = shift_list(1+(dist/20))
        elif dist <= 600:
            shift_amt = shift_list(dist/15)
        elif dist <= 1000:
            shift_amt = shift_list(600/15 + (dist-600)/11.428)
    return (shift_time(start_time, shift_amt)).isoformat()
