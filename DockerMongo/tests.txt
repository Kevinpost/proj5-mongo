TESTS:

-NO ENTRIES:
Expected behavior: Nothing will be in DATABASE after submit & display

-DATE is changed or TIME is changed or BREVET DISTANCE is changed:
Expected behavior:  ONLY the modified values should make it in the database

-"Hard" refreshing of the page (Ctrl+shift+R or similar command)
Expected behavior:  Database should be emptied, none of these entries should carry to "display" page even if "submit" button has been pushed

-Controle points entered, then display button clicked BEFORE submit button not clicked:
Expected behavior: No entries should be displayed.
