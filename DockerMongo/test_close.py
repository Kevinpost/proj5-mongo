"""
Nose tests
"""
from acp_times import close_time, open_time
import arrow
import nose    # Testing framework
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)

import time
def same(s, t):
    """
    Two 8601 strings are the same-ish or not.
    """
    #print(s)
    #print(t)
    return s[:16] == t[:16]


def test_close_time_zero():
    """Tests close time using french rules when start time < 60km"""
    date_time_string = "2017-01-01 00:00:00"
    date_time_arrow = arrow.get(date_time_string, 'YYYY-MM-DD HH:mm:ss')
    assert same(str(close_time(40, 200, date_time_arrow.isoformat())), "2017-01-01T03:00:00")

def test_close_time_at_max_200():
    """Tests close time @ 200 km in 200km brevet.  should be +13:30 not +13:20"""
    date_time_string = "2017-01-01 00:00:00"
    date_time_arrow = arrow.get(date_time_string, 'YYYY-MM-DD HH:mm:ss')
    assert same(str(close_time(200, 200, date_time_arrow.isoformat())), "2017-01-01T13:30:00")

def test_close_time_at_max_400():
    """Tests close time @ 400 km in 400km brevet.  should be +27:00 hours not + 26:40"""
    date_time_string = "2017-01-01 00:00:00"
    date_time_arrow = arrow.get(date_time_string, 'YYYY-MM-DD HH:mm:ss')
    assert same(str(close_time(400, 400, date_time_arrow.isoformat())), "2017-01-02T03:00:00")

def test_close_time_past_max_1100_in_1000():
    """Tests close time past max.  should be + 3 days 3 hours"""
    date_time_string = "2017-01-01 00:00:00"
    date_time_arrow = arrow.get(date_time_string, 'YYYY-MM-DD HH:mm:ss')
    assert same(str(close_time(1100, 1000, date_time_arrow.isoformat())), "2017-01-04T03:00:00")

def test_close_time_600_in_1000():
    """different name"""
    date_time_string = "2017-01-01 00:00:00"
    date_time_arrow = arrow.get(date_time_string, 'YYYY-MM-DD HH:mm:ss')
    assert same(close_time(600, 1000, date_time_arrow.isoformat()), "2017-01-02T16:00:00")
